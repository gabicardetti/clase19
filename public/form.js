function formSubmit(event) {
  event.preventDefault();

  const title = document.getElementById("fTitle").value;
  const price = document.getElementById("fPrice").value;
  const thumbnail = document.getElementById("fThumbnail").value;
  const body = {
    title,
    price,
    thumbnail,
  };

  const url = "/api/productos/guardar";
  const request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  request.onload = function () {
    document.getElementById("fTitle").value = "";
    document.getElementById("fPrice").value = "";
    document.getElementById("fThumbnail").value = "";
    alert("Producto creado con exito");
  };

  request.onerror = function () {};

  request.send(JSON.stringify(body));
}
document.getElementById("form").addEventListener("submit", formSubmit);
