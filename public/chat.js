const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
const formMessage = document.getElementById("formMessage");
const inputMessage = document.getElementById("inputMessage");

const inputEmail = document.getElementById("inputEmail");

formMessage.addEventListener("submit", function (e) {
  e.preventDefault();
  const email = inputEmail.value;
  if (!validateEmail(email)) {
    alert("email no valido");
    return;
  }

  if (inputMessage.value) {
    const message = {
      date: new Date(),
      email,
      message: inputMessage.value,
    };
    socket.emit("new message", message);
    inputMessage.value = "";
  }
});

socket.on("messages", (msg) => {
    let newMessage = "";
    newMessage +=` <span class="email"> ${msg.email} </span>`
    newMessage +=` <span class="date"> [${msg.date}]:  </span>`
    newMessage +=` <span class="message" > ${msg.message} </span>`


  document.getElementById("messageBody").innerHTML += newMessage + "<br>";
});
