import Message from "../models/Message.js"

export default class ChatRepositoryMongo {

    constructor() {
    }

    async getAll() {
        return Message.find();
    }

    async save(dto) {
        const message = new Message({
            email: dto.email,
            date: dto.date,
            message: dto.message
        });

        return message.save();
    }

}
