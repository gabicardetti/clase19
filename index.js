import express from "express";
import mongoose from "mongoose";
import ProductRoutes from "./src/routes/productRoutes.js";
import ChatRoutes from "./src/routes/chatRoutes.js";

import { getAllProducts } from "./src/services/productService.js";
import { saveNewMessage } from ".//src/services/chatService.js"

import { Server } from "socket.io";
import http from "http";

const app = express();
const port = 8080;
const baseUrl = "/api";

const server = http.createServer(app);
const io = new Server(server);
app.locals.io = io;

io.on("connection", async (socket) => {
  console.log("a user connected");

  const products = await getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);

try {
  mongoose.connect("mongodb://localhost:27017/eccomerce", { useNewUrlParser: true })
} catch (e) {
  console.log(e)
  throw new Error("mongo db cannot be connected");
}

server.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});
